from enum import Enum

from django.db import models
from ordered_model.models import OrderedModel


class Status(Enum):
    OK = 'ok'
    DOWN = 'down'
    UNKNOWN = 'unknown'


class Service(OrderedModel):
    short_name = models.CharField(null=False, max_length=64)
    image = models.ImageField(null=True, blank=True, upload_to='services')
    description = models.TextField(null=True, blank=True)
    port = models.IntegerField(null=True, blank=True, help_text="Used for checking status")
    url = models.URLField()

    def __str__(self):
        return f"{self.short_name} ({self.url})"


class Link(OrderedModel):
    short_name = models.CharField(null=False, max_length=64)
    image = models.ImageField(null=True, blank=True, upload_to='links')
    description = models.TextField(null=True, blank=True)
    url = models.URLField()

    def __str__(self):
        return f"{self.short_name} ({self.url})"

    class Meta(OrderedModel.Meta):
        verbose_name = "Important Link"
        verbose_name_plural = "Important Links"


class Machine(OrderedModel):
    short_name = models.CharField(null=False, max_length=64)
    image = models.ImageField(null=True, blank=True, upload_to='machines')
    description = models.TextField(null=True, blank=True)
    url = models.URLField(null=True, blank=True)
    healthcheck_id = models.CharField(null=True, blank=True, max_length=36)

    def __str__(self):
        return f"{self.short_name}"


class Feed(models.Model):
    short_name = models.CharField(null=True, blank=True, max_length=64)
    url = models.URLField()

    def __str__(self):
        return f"{self.short_name} ({self.url})" if self.short_name else self.url
