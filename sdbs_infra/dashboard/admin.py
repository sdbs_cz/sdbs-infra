from django.contrib import admin
from ordered_model.admin import OrderedModelAdmin

from sdbs_infra.dashboard.models import Service, Link, Machine, Feed


class LinkAdmin(OrderedModelAdmin):
    list_display = ('short_name', 'url', 'move_up_down_links')


class ServiceAdmin(OrderedModelAdmin):
    list_display = ('short_name', 'url', 'move_up_down_links')


class MachineAdmin(OrderedModelAdmin):
    list_display = ('short_name', 'move_up_down_links')


admin.site.register(Link, LinkAdmin)
admin.site.register(Service, ServiceAdmin)
admin.site.register(Machine, MachineAdmin)
admin.site.register(Feed)
