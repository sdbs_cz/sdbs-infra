from django.apps import AppConfig


class DashboardConfig(AppConfig):
    name = 'sdbs_infra.dashboard'
