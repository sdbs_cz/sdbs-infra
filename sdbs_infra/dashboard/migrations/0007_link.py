# Generated by Django 3.0.7 on 2020-06-20 17:17

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('dashboard', '0006_service_port'),
    ]

    operations = [
        migrations.CreateModel(
            name='Link',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('order', models.PositiveIntegerField(db_index=True, editable=False, verbose_name='order')),
                ('short_name', models.CharField(max_length=64)),
                ('image', models.ImageField(blank=True, null=True, upload_to='links')),
                ('description', models.TextField(blank=True, null=True)),
                ('url', models.URLField()),
            ],
            options={
                'verbose_name': 'Important Link',
                'verbose_name_plural': 'Important Links',
            },
        ),
    ]
